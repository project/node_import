Drupal node_import.module README.txt
==============================================================================

Currently there is no HEAD version of the Node import module. Please use the
latest stable release from http://drupal.org/project/node_import for your
specific Drupal version.

See http://drupal.org/node/17570 as to why this approach was taken.

A new HEAD version of node_import compatible with Drupal 7.x will be created
once Drupal 7.x is considered stable.

Credits / Contact
------------------------------------------------------------------------------

The original author (version 4.5) of this module is Moshe Weitzman (weitzman).
Neil Drumm (drumm) rewrote the module for 4.6 (which he never released as
such).

The port of the module to 4.7 and some new features were provided by:
 - David Donohue (dado),
 - Nic Ivy (njivy).

Robrecht Jacques (robrechtj) is the current active maintainer of the module.
Both 5.x and 6.x releases are out.

Best way to contact the developers to report bugs or feature requests is by
visiting the node_import project page at http://drupal.org/project/node_import.

